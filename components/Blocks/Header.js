import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity, Text} from 'react-native';
import {colors, fonts} from '../assists/Colors_Fonts';

export default class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
    };
  }

  async componentDidMount() {}

  render() {
    return (
      <View style={styles.container}>
        
        {this.props.hideBack ? (
          <View style={styles.headerLeft} />
        ) : (
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.goBack(null);
            }}
            style={styles.headerLeft}>
            <Image
              style={styles.backIcon}
              source={require('../assists/img/backEn.png')}
            />
          </TouchableOpacity>
        )}
        <View style={styles.headerCenter}>
          <Text style={styles.title}>{this.props.title}</Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('Search');
          }}
          style={styles.headerRight}>
          <Image
            style={styles.seachIcon}
            source={require('../assists/img/search.png')}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    flexDirection: 'row',
    padding: 15,
  },
  headerLeft: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    width: '25%',
  },
  headerRight: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    width: '25%',
  },
  headerCenter: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
  },
  backIcon: {
    width: 20,
    height: 20,
    tintColor: 'white',
  },

  seachIcon: {
    width: 30,
    height: 30,
    tintColor: 'white',
  },
  title: {
    fontFamily: fonts.SemiBold,
    fontSize: 18,
    color: 'white',
  },
});
