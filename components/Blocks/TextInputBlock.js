import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

// colors & fonts
import {fonts, colors} from '../assists/Colors_Fonts';

export default class TextInputBlock extends Component {
  state = {
    isFocus: false,
  };
  render() {
    return (
      <View style={styles.TextInputs_outer}>
        {this.props.label ? (
          <View style={styles.label_outer}>
            <Text style={styles.label_text}>{this.props.label}</Text>
          </View>
        ) : null}
        <View style={styles.TextInputs_inner}>
          <View style={styles.TextInput_outer}>
            <TextInput
              style={styles.TextInputEn}
              value={this.props.value}
              editable={this.props.editable}
              onChangeText={this.props.onChangeText}
            />
          </View>

          <TouchableOpacity
            onPress={this.props.onSecurePress}
            style={styles.TextInput_outer_search}>
            <Image
              source={require('../assists/img/search.png')}
              style={styles.icon}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  TextInputs_outer: {
    width: '100%',
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },

  TextInputs_inner: {
    width: '100%',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#E0E0E0',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    backgroundColor: 'white',
  },

  TextInput_outer: {
    width: '80%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  TextInput_outer_search: {
    width: '20%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  icon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    tintColor: colors.mainColor,
  },

  TextInputEn: {
    width: '100%',
    color: colors.mainColor,
    fontFamily: fonts.Regular,
    paddingVertical: 0,
    paddingHorizontal: 5,
    textAlign: 'left',
  },

  label_outer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingVertical: 5,
  },

  label_text: {fontFamily: fonts.Regular, color: 'white', fontSize: 12},
});
