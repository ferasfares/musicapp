import React, {Component} from 'react';
import {Text, View, ActivityIndicator, Modal, StyleSheet} from 'react-native';

import {fonts, colors} from '../assists/Colors_Fonts';
export default class PageLoader extends Component {
  render() {
    if (this.props.flatlist) {
      return (
        <View style={styles.FlatListFooter}>
          <ActivityIndicator color={colors.pink} size={48} />
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <ActivityIndicator color={colors.pink} size={48} />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    flex: 1,
    backgroundColor: colors.mainColor,
  },
  FlatListFooter: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    flex: 1,
  },
});
