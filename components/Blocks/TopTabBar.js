import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity, Text} from 'react-native';
import {colors, fonts} from '../assists/Colors_Fonts';

export default class TopTabBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
    };
  }

  async componentDidMount() {}

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('Search');
          }}
          style={
            this.props.active == 1 ? styles.tabInnerActive : styles.tabInner
          }>
          <Image
            style={styles.seachIcon}
            source={require('../assists/img/track.png')}
          />
          <Text style={styles.title}>Track</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('Search');
          }}
          style={styles.tabInner}>
          <Image
            style={styles.seachIcon}
            source={require('../assists/img/artist.png')}
          />
          <Text style={styles.title}>Artists</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('Search');
          }}
          style={styles.tabInner}>
          <Image
            style={styles.seachIcon}
            source={require('../assists/img/album.png')}
          />
          <Text style={styles.title}>Albums</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    flexDirection: 'row',
    padding: 15,
    backgroundColor:colors.mainColor,
    elevation:15
  },

  tabInner: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '33.3%',
  },

  tabInnerActive: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '33.3%',
    backgroundColor: colors.pink,
  },
  backIcon: {
    width: 20,
    height: 20,
    tintColor: 'white',
  },

  seachIcon: {
    width: 30,
    height: 30,
    tintColor: 'white',
  },
  title: {
    fontFamily: fonts.SemiBold,
    fontSize: 18,
    color: 'white',
  },
});
