import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity, Text} from 'react-native';
import {colors, fonts} from '../assists/Colors_Fonts';
import FastImage from 'react-native-fast-image';
export default class TrackBlock extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
    };
  }

  async componentDidMount() {}

  render() {
    const item = this.props.item;
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.navigation.navigate('PlaySound');
        }}
        style={styles.outer}>
        <View style={styles.inner}>
          <View style={styles.left}>
            <Image
              style={{width: 60, height: 60,tintColor:'white'}}
              source={require('../screens/resources/ui_speaker.png')}
            />
          </View>

          <View style={styles.right}>
            <Text style={styles.name}>{item.track}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  outer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    padding: 5,
  },

  inner: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    padding: 10,
    borderWidth: 1,
    borderColor: colors.pink,
    borderRadius: 10,
    flexDirection: 'row',
  },

  left: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '25%',
  },

  right: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '75%',
  },
  name: {
    fontFamily: fonts.SemiBold,
    fontSize: 18,
    color: 'white',
  },
});
