import React, {Component} from 'react';

const global = require('../assists/Global.json');

// Stack
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

// ============================== Screens ==============================//
// Home Screens
import Home from '../screens/HomePage';
import Album from '../screens/Album';
import Track from '../screens/Track';
import PlaySound from '../screens/PlaySound';
import Search from '../screens/Search';

export default class ScreenStack extends Component {
  render() {
    return (
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerShown: false,
          presentation: 'card',
        }}>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Album" component={Album} />
        <Stack.Screen name="Track" component={Track} />
        <Stack.Screen name="PlaySound" component={PlaySound} />
        <Stack.Screen name="Search" component={Search} />

      </Stack.Navigator>
    );
  }
}
