import React from 'react';
import {View, Text, FlatList, TextInput} from 'react-native';
import styles from '../assists/Styles';

import Header from '../Blocks/Header';
import Loader from '../Blocks/Loader';
import Artist from '../Blocks/Artist';
import TextInputBlock from '../Blocks/TextInputBlock';
import {tsNonNullExpression} from '@babel/types';

const global = require('../assists/Global.json');

export default class Search extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      Data: [],
    };
  }

  async componentDidMount() {}

  async getSearchResult(SearchKey) {
    fetch(
      global.api_link +
        '?q=' +
        SearchKey +
        '&limit=10&apikey=' +
        global.apiKey +
        '&type=artist&lyrics=0',
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      },
    )
      .then(response => response.json())
      .then(res => {
        //success
        this.setState({
          Data: res.result,
          isLoading: false,
        });
      })
      .catch(error => {});
  }

  render() {
    if (this.state.isLoading) {
      return <Loader />;
    } else {
      return (
        <View style={styles.container}>
          <Header navigation={this.props.navigation} title={'Search'} />
          <TextInputBlock
            onChangeText={SearchKey => {
              if (SearchKey == '') {
                this.setState({
                  SearchKey: null,
                  Data: [],
                });
              } else {
                this.setState(
                  {
                    SearchKey: SearchKey,
                 
                  },
                  () => {
                    this.getSearchResult(SearchKey);
                  },
                );
              }
            }}
            value={this.state.Email}
            placeholder={'feras'}
            label={'search by artists name'}
          />

          {this.state.SearchKey != '' && this.state.SearchKey ? (
            <FlatList
              style={{flex: 1}}
              maxToRenderPerBatch={10}
              initialNumToRender={14}
              shouldComponentUpdate={() => {
                return false;
              }}
              onEndReachedThreshold={0.1}
              legacyImplementation={true}
              bounces={false}
              disableVirtualization
              removeClippedSubviews={true}
              ListEmptyComponent={this.ListEmptyComponent()}
              data={this.state.Data}
              renderItem={this._renderListItem}
              keyExtractor={(item, index) => index.toString()}
            />
          ) : null}
        </View>
      );
    }
  }

  ListEmptyComponent() {
    return (
      <View style={styles.noResultContainer}>
        <Text allowFontScaling={false} style={styles.noResultText}>
          There is no result
        </Text>
      </View>
    );
  }

  _renderListItem = ({item, index}) => {
    return (
      <Artist
        key={item.id_artist}
        navigation={this.props.navigation}
        item={item}
      />
    );
  };
}
