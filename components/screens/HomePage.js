import React from 'react';
import {View, Text, FlatList} from 'react-native';
import styles from '../assists/Styles';

import Header from '../Blocks/Header';
import Loader from '../Blocks/Loader';
import Artist from '../Blocks/Artist';
const global = require('../assists/Global.json');

export default class HomePage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      Data: [],
      page: 1,
      isNewDataLoader: false,
    };
  }

  async componentDidMount() {
    this.getArtists();
  }

  async getArtists() {
    fetch(
      global.api_link +
        'artists?page=' +
        this.state.page +
        '&apikey=' +
        global.apiKey,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      },
    )
      .then(response => response.json())
      .then(res => {
        if (res.result.length > 0) {
          //success
          this.setState({
            Data: this.state.Data.concat(res.result),
            isLoading: false,
            DataSuccess: true,
            isNewDataLoader: false,
          });
        } else {
          this.setState({
            isLoading: false,
            isNewDataLoader: false,
            DataSuccess: false,
          });
        }
      })
      .catch(error => {});
  }

  render() {
    if (this.state.isLoading) {
      return <Loader />;
    } else {
      return (
        <View style={styles.container}>
          <Header navigation={this.props.navigation} title={'Artists'} hideBack />

          <FlatList
            style={{flex: 1}}
            maxToRenderPerBatch={10}
            initialNumToRender={14}
            shouldComponentUpdate={() => {
              return false;
            }}
            onEndReachedThreshold={0.1}
            legacyImplementation={true}
            bounces={false}
            disableVirtualization
            removeClippedSubviews={true}
            ListEmptyComponent={this.ListEmptyComponent()}
            data={this.state.Data}
            renderItem={this._renderListItem}
            keyExtractor={(item, index) => index.toString()}
            onEndReached={({distanceFromEnd}) => {
              this.OnEndReached();
            }}
            ListFooterComponent={this.FlatList_Footer()}
            ListFooterComponentStyle={{width: '100%'}}
          />
        </View>
      );
    }
  }

  ListEmptyComponent() {
    return (
      <View style={styles.noResultContainer}>
        <Text allowFontScaling={false} style={styles.noResultText}>
          There is no result
        </Text>
      </View>
    );
  }

  _renderListItem = ({item, index}) => {
    return (
      <Artist
        key={item.id_artist}
        navigation={this.props.navigation}
        item={item}
      />
    );
  };

  async OnEndReached() {
    if (this.state.DataSuccess) {
      this.setState(
        {
          DataSuccess: false,
          page: this.state.page + 1,
          isNewDataLoader: true,
        },
        () => {
          this.getArtists();
        },
      );
    }
  }

  FlatList_Footer() {
    return <Loader flatlist />;
  }
}
