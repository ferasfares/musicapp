import React from 'react';
import {View, Text, FlatList} from 'react-native';
import styles from '../assists/Styles';

import Header from '../Blocks/Header';
import Loader from '../Blocks/Loader';
import TrackBlock from '../Blocks/TrackBlock';
const global = require('../assists/Global.json');

export default class Track extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      Data: [],
      id_album: props.route.params.id_album,
      id_artist: props.route.params.id_artist,
    };
  }

  async componentDidMount() {
    this.getArtists();
  }

  async getArtists() {
    fetch(
      global.api_link +
        'artists/' +
        this.state.id_artist +
        '/albums/' +
        this.state.id_album +
        '/tracks?apikey=' +
        global.apiKey,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      },
    )
      .then(response => response.json())
      .then(res => {
        //success
        this.setState({
          Data: res.result.tracks,
          isLoading: false,
        });
      })
      .catch(error => {});
  }

  render() {
    if (this.state.isLoading) {
      return <Loader />;
    } else {
      return (
        <View style={styles.container}>
          <Header navigation={this.props.navigation} title={'Track'} />

          <FlatList
            style={{flex: 1}}
            maxToRenderPerBatch={10}
            initialNumToRender={14}
            shouldComponentUpdate={() => {
              return false;
            }}
            onEndReachedThreshold={0.1}
            legacyImplementation={true}
            bounces={false}
            disableVirtualization
            removeClippedSubviews={true}
            ListEmptyComponent={this.ListEmptyComponent()}
            data={this.state.Data}
            renderItem={this._renderListItem}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      );
    }
  }

  ListEmptyComponent() {
    return (
      <View style={styles.noResultContainer}>
        <Text allowFontScaling={false} style={styles.noResultText}>
          There is no result
        </Text>
      </View>
    );
  }

  _renderListItem = ({item, index}) => {
    console.log(item)
    if (item.haslyrics || true) {
      return (
        <TrackBlock
          key={item.id_track}
          navigation={this.props.navigation}
          item={item}
        />
      );
    }
  };
}
