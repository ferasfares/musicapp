import React, {Component} from 'react';
import {View, StatusBar, Image} from 'react-native';

export default class NoConnection extends Component {
  render() {
    return (
      <View style={{backgroundColor: '#f6f6f6'}}>
        <StatusBar backgroundColor={'#f6f6f6'} hidden={true} />
        <Image
          source={require('../assists/img/cloud.jpg')}
          style={{width: '100%', height: '100%', resizeMode: 'contain'}}
        />
      </View>
    );
  }
}
