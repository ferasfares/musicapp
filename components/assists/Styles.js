import {StyleSheet, Dimensions} from 'react-native';
import {colors, fonts} from '../assists/Colors_Fonts';
let deviceHeight = Dimensions.get('screen').height;
var deviceWidth = Dimensions.get('screen').width;
const styles = StyleSheet.create({
  // ===============================================//
  //                    All Screens
  // ===============================================//
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor:colors.mainColor
  },

  // ===============================================//
  //                   top tab bar
  // ===============================================//

  tabBarIcon:{
    width:20,
    height:20
  },

  // ===============================================//
  //                 flatlist
  // ===============================================//
  noResultText: {
    fontFamily: fonts.Light,
    fontSize: 18,
    color: 'white',
  },
  noResultContainer: {
    width: '100%',
    height: '100%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  
 flatListContainer: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

});

export default styles;
