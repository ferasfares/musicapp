export const colors = {
  mainColor: '#181e21',
  pink:'#e62b7a'


};

export const fonts = {
  SemiBold: 'Cairo-SemiBold',
  Regular: 'Cairo-Regular',
  Light: 'Cairo-Light',
  Bold: 'Cairo-Bold',
};
