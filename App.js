import React, {Component} from 'react';
import {SafeAreaView, LogBox, StatusBar} from 'react-native';
import ScreenStack from './components/navigations/ScreenStack';

LogBox.ignoreAllLogs(true);
import NetInfo from '@react-native-community/netinfo';
import NoConnection from './components/screens/NoConnection';
import {enableScreens} from 'react-native-screens';
import {NavigationContainer} from '@react-navigation/native';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      isConnected: true,
    };
  }

  componentDidMount() {
    enableScreens();
    NetInfo.addEventListener(state => {
      this.setState({isConnected: state.isConnected});
    });
  }

  render() {
    if (this.state.isConnected) {
      return (
        <>
          <StatusBar
            barStyle="light-content"
            backgroundColor={'#181e21'}
          />
          <NavigationContainer
           ref={'navigationRef'} 
          >
            <SafeAreaView style={{flex: 1}}>
              <ScreenStack />
            </SafeAreaView>
          </NavigationContainer>
        </>
      );
    } else {
      return <NoConnection />;
    }
  }
}

export default App;
